const { Citas } = require("../models/Citas");

const paginaPresentacion = (req, res) => {
    res.render('presentacion');
}

const paginaCrearCita = (req, res) => {
    res.render('crearCita');
}

const paginaVercitas = async (req, res) => {
    try {
        const citas = await Citas.findAll();

        res.render('verCitas', {
            citas
        });

    } catch (error) {
        console.log(error);
    }
}

const crearCita = async (req, res) => {
    const { nombre, sintomas, fecha_cita, nombre_propietario } = req.body;
    try {
        await Citas.create({
            nombre,
            sintomas,
            fecha_cita,
            nombre_propietario
        });

        res.redirect('/verCitas');

    } catch (error) {
        console.log(error);
    }
}

const paginaEditarCita = async(req, res) => {
    const { id } = req.params;
    try {
        const cita = await Citas.findOne({ where: { id } });

        res.render('editarCita', {
            cita
        });

    } catch (error) {
        console.log(error);
    }
}

const editarCita = async(req, res) => {
    const { id } = req.params;
    const { nombre, sintomas, fecha_cita, nombre_propietario } = req.body;

    try {
        await Citas.update({
            nombre,
            sintomas,
            fecha_cita,
            nombre_propietario
        },{
            where: {
                id
            }
        });

        res.redirect('/verCitas');

    } catch (error) {
        console.log(error);
    }
}

const eliminarCita = async(req, res) => {
    const { id } = req.params;
    try {
        await Citas.destroy({
            where: {
                id
            }
        });

        res.redirect('/verCitas');

    } catch (error) {
        console.log(error);
    }

}
module.exports = {
    paginaPresentacion,
    paginaCrearCita,
    paginaVercitas,
    crearCita,
    paginaEditarCita,
    editarCita,
    eliminarCita
}