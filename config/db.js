const { Sequelize } = require('sequelize');

const db = new Sequelize('proyecto_citas', 'root', '',{
    host: 'localhost',
    dialect: 'mysql',
    port: '3306',
    define: {
        timestamps: false
    },
    pool:{
        max: 5,
        min: 0,
        acquire: 3000,
        idle: 10000
    }
});

module.exports = db;