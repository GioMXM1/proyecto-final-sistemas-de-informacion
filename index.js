const express = require('express');
const router = require('./routers/citas.router');
const db = require('./config/db');

const app = express();

db.authenticate()
    .then(res => {
        console.log('DB Lista');
    });

app.set('view engine','pug');

app.use(express.json());

app.use(express.urlencoded({extended:true }));

app.use('/', router);

app.listen(4000, () => {
    console.log('Servidor corriendo en el puerto: 4000');
});





