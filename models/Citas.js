const { DataTypes } = require("sequelize");
const db = require('../config/db');

const Citas = db.define('citas',{
    id:{
        type: DataTypes.INTEGER,
        primaryKey: true
    },
    nombre:{
        type: DataTypes.STRING
    },
    sintomas:{
        type: DataTypes.STRING
    },
    fecha_cita:{
        type: DataTypes.DATE
    },
    nombre_propietario:{
        type: DataTypes.STRING
    }
});

module.exports = {
    Citas
}