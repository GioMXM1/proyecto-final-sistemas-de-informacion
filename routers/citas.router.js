const { Router } = require('express');
const { 
    paginaCrearCita, 
    paginaVercitas, 
    crearCita, 
    paginaEditarCita, 
    editarCita, 
    eliminarCita, 
    paginaPresentacion 
} = require('../controllers/citas.controller');

const router = Router();

router.get('/', paginaPresentacion);

router.get('/crearCita', paginaCrearCita);

router.post('/crearCita', crearCita)

router.get('/verCitas', paginaVercitas);

router.get('/edit/:id', paginaEditarCita)

router.post('/edit/:id', editarCita)

router.get('/delete/:id', eliminarCita)


module.exports = router;